export default {
  APP_NAME: "DASHBOARD",
  ///////////////////////////////////////////////////
  APP_PRICING_NAME: "Planes",
  APP_PRICING_TITLE: "Elige el mejor plan para ti",
  APP_PRICING_SUBTITLE:
    "Todas las opciones de disponibles para empresas y particulares",

  APP_PRICING_CARD_1_TITLE: "BÁSICO",
  APP_PRICING_CARD_1_ICON: "free_breakfast",
  APP_PRICING_CARD_1_PRICE: "4 €/mes",
  APP_PRICING_CARD_1_BODY_1: "30 MB tamaño de archivo",
  APP_PRICING_CARD_1_BODY_2: "1.5 GB tráfico mensual",
  APP_PRICING_CARD_1_BODY_3: "Wordpress Free Plugin",
  APP_PRICING_CARD_1_BODY_4: "Acceso web app",
  APP_PRICING_CARD_1_BUTTON: "!Me interesa!",

  APP_PRICING_CARD_2_TITLE: "ADVANCED",
  APP_PRICING_CARD_2_ICON: "workspace_premium",
  APP_PRICING_CARD_2_PRICE: "25 €/mes",
  APP_PRICING_CARD_2_BODY_1: "50 MB tamaño de archivo",
  APP_PRICING_CARD_2_BODY_2: "10 GB tráfico mensual",
  APP_PRICING_CARD_2_BODY_3: "Wordpress Plugin PRO",
  APP_PRICING_CARD_2_BODY_4: "Acceso web app",
  APP_PRICING_CARD_2_BODY_5: "2x API endopoints",
  APP_PRICING_CARD_2_BUTTON: "!Lo quiero!",

  APP_PRICING_CARD_3_TITLE: "CORPORATE",
  APP_PRICING_CARD_3_ICON: "business",
  APP_PRICING_CARD_3_PRICE: "45 €/mes",
  APP_PRICING_CARD_3_BODY_1: "50 MB tamaño de archivo",
  APP_PRICING_CARD_3_BODY_2: "100 GB tráfico mensual",
  APP_PRICING_CARD_3_BODY_3: "Wordpress Plugin PRO",
  APP_PRICING_CARD_3_BODY_4: "Acceso web app",
  APP_PRICING_CARD_3_BODY_5: "10x API endopoints",
  APP_PRICING_CARD_3_BUTTON: "!Este es para mi!",

  APP_DASHBOARD_TABLE_COL0: "Dominio",
  APP_DASHBOARD_TABLE_COL1: "Válido hasta",
  APP_DASHBOARD_TABLE_COL2: "Abrir",
  APP_DASHBOARD_TABLE_COL3: "Plan",

  APP_DASHBOARD_TABLE_PLAN_UPGRADE: "Actualizar",

  APP_ADD_SITE_BUTTON_LABEL: "Añadir dominio",
  APP_PLUGIN_DOWNLOAD_TITLE: "Empieza a limpiar metadatos a tu manera:",
  APP_PLUGIN_DOWNLOAD_BUTTON_LABEL: "Descargar Plugin",
  APP_PLUGIN_WORDPRESS_TITLE: "Wordpress",

  APP_MY_SITE_TITLE: "Introduce el nombre de tu dominio o tu email.",
  APP_ADD_SITE_FORM_DOMAIN: "Ejemplo: www.example.com, hola@example.com",

  APP_MY_SITE_TOKEN_TITLE: "Tu Token",
  APP_MY_SITE_TOKEN_HELP_TEXT:
    "Este token te permitirá utilizar los plugins disponibles en cualquiera de tus plataformas",
  APP_MY_SITE_BUTTON_SAVE_TEXT: "Guardar",
  APP_MY_SITE_BUTTON_TOKEN_TEXT: "Tu token es...",
  APP_MY_SITE_BUTTON_UPGRADE_TEXT: "Actualiza tu plan",

  APP_MY_SITE_UPLOAD_TITLE: "Sube aquí los archivos que quieres limpiar",
  APP_MY_SITE_UPLOAD_SUBTITLE: "Tu Token",

  APP_COPY_NOTIFICATION_MESSAGE: "Texto copiado al portapapeles",
  APP_SAVE_NOTIFICATION_MESSAGE: "Cambios guardados con éxito",
  APP_BF_ADD_DOMAIN_ENTER_DOMAIN: "Introduce un dominio para obtener tu token",
  APP_BF_ADD_DOMAIN_TEXT_COPIED: "Texto copiado al portapapeles",
  APP_BF_ADD_DOMAIN_ERROR_COPY_TEXT:
    "No se pudo copiar el texto al portapapeles",
  APP_BF_ADD_DOMAIN_COPY_TOKEN: "Copiar token"
};
