export default {
  APP_NAME: "DASHBOARD",
  ///////////////////////////////////////////////////
  APP_PRICING_NAME: "Plans",
  APP_PRICING_TITLE: "Choose the best plan for you",
  APP_PRICING_SUBTITLE: "Ble ble ble ble ble",

  APP_PRICING_CARD_1_TITLE: "BASIC",
  APP_PRICING_CARD_1_ICON: "free_breakfast",
  APP_PRICING_CARD_1_PRICE: "4 €/month",
  APP_PRICING_CARD_1_BODY_1: "30 MB max size per file",
  APP_PRICING_CARD_1_BODY_2: "1.5 GB Monthly traffic",
  APP_PRICING_CARD_1_BODY_3: "Wordpress Free Plugin",
  APP_PRICING_CARD_1_BODY_4: "Web app access",
  APP_PRICING_CARD_1_BUTTON: "Go for it!",

  APP_PRICING_CARD_2_TITLE: "ADVANCED",
  APP_PRICING_CARD_2_ICON: "workspace_premium",
  APP_PRICING_CARD_2_PRICE: "25 €/month",
  APP_PRICING_CARD_2_BODY_1: "50 MB max size per file",
  APP_PRICING_CARD_2_BODY_2: "10 GB Monthly traffic",
  APP_PRICING_CARD_2_BODY_3: "Wordpress Plugin PRO",
  APP_PRICING_CARD_2_BODY_4: "Web app access",
  APP_PRICING_CARD_2_BODY_4: "2x API endopoints",
  APP_PRICING_CARD_2_BUTTON: "I want it!",

  APP_PRICING_CARD_3_TITLE: "CORPORATE",
  APP_PRICING_CARD_3_ICON: "business",
  APP_PRICING_CARD_3_PRICE: "45 €/month",
  APP_PRICING_CARD_3_BODY_1: "50 MB max size per file",
  APP_PRICING_CARD_3_BODY_2: "100 GB Monthly traffic",
  APP_PRICING_CARD_3_BODY_3: "Wordpress Plugin PRO",
  APP_PRICING_CARD_3_BODY_4: "Web app access",
  APP_PRICING_CARD_3_BODY_4: "10x API endopoints",
  APP_PRICING_CARD_3_BUTTON: "That's for me"
};
